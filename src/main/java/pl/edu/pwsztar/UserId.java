package pl.edu.pwsztar;

import java.util.Optional;

final class UserId implements UserIdChecker {

    private final String id;    // NR. PESEL

    public UserId(final String id) {
        this.id = id;
    }

    @Override
    public boolean isCorrectSize() {
        return  id.length() == 11 && id != null;
    }

    @Override
    public Optional<Sex> getSex() {
        if (isCorrect()) {
            return Character.getNumericValue(id.charAt(9)) % 2 == 1 ? Optional.of(Sex.MAN) : Optional.of(Sex.WOMAN);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public boolean isCorrect() {
        if(!isCorrectSize() || !id.matches("[0-9]+")){
            return false;
        }
        int[] checkSum = new int[] {9,7,3,1,9,7,3,1,9,7};
        int sum = 0;
        for(int i = 0; i < 10 ; i++) {
            sum += Character.getNumericValue(id.charAt(i)) * checkSum[i];
        }

        return sum % 10 == Character.getNumericValue(id.charAt(10));
    }

    @Override
    public Optional<String> getDate() {
        if(!isCorrect()){
            return Optional.empty();
        }

        int yearCheck = Integer.parseInt(String.valueOf(id.charAt(2)));
        String day = id.substring(4,6);
        String month = id.substring(3,4);
        int year = Integer.parseInt(id.substring(0,2));

        if(yearCheck == 0 || yearCheck == 1){
            month = yearCheck + month;
        }else{
            month = "0" + month;
        }
        if(yearCheck == 0 || yearCheck == 1){
            year += 1900;
        }else if(yearCheck == 2 || yearCheck == 3){
            year += 2000;
        }else if(yearCheck == 4 || yearCheck == 5){
            year += 2100;
        }else if(yearCheck == 6 || yearCheck == 7){
            year += 2200;
        }else if(yearCheck == 8 || yearCheck == 9){
            year += 1800;
        }

        return Optional.of(day + "-" + month + "-" + year);
    }
}
