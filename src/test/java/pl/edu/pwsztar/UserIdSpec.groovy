package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class UserIdSpec extends Specification {

    @Unroll
    def "should check correct size for #id"() {
        when:
            UserId userId = new UserId(id)
        then:
            expectedResult == userId.isCorrectSize()
        where:
            id              |  expectedResult
            "97110585954"   |  true
            ""              |  false
            "123456789"     |  false
            "44051401458143"   |  false
    }
    @Unroll
    def "should check sex for #id"() {
        when:
            UserId userId = new UserId(id)
        then:
            expectedResult == userId.getSex().orElse(null)
        where:
            id              | expectedResult
            '97110585954'   | UserIdChecker.Sex.MAN
            '89092692348'   | UserIdChecker.Sex.WOMAN
            '123456789'     | null
    }

    @Unroll
    def "should check correct PESEL = #id"() {
        when:
        def userId  = new UserId(id)
        then:
        expectedResult == userId.isCorrect()
        where:
        id                  | expectedResult
        '97110585954'       | true
        '59120666788'       | true
        'qwert123443'       | false
        '134134'            | false
    }

    @Unroll
    def "should return date from PESEL = #id"() {
        when:
        def userId  = new UserId(id)
        then:
        expectedResult == userId.getDate().orElse(null)
        where:
        id                  | expectedResult
        '77090269231'       | '02-09-1977'
        '97110585954'       | '05-11-1997'
        'qwert123443'       | null
        '134134'            | null
    }


}
